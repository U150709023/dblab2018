select count(customers.CustomerID) as numberOfCustomers, customers.Country
from customers
group by Country
order by numberOfCustomers desc;


select count(products.ProductID), suppliers.SupplierName
from products join suppliers on products.SupplierID = suppliers.SupplierID
group by products.SupplierID
order by count(products.ProductID) desc;



use company;
load data local infile "C:\\Users\\ASUS\\Downloads\\customers.csv"
into table customers 
fields terminated by ';'
ignore 1 lines;

select * from customers;